// External imports
import express, { Request, Response, NextFunction } from 'express';

// Local imports
import UtxoClient from '../clients/utxoClient';
import ErrorHandler from '../common/error-handler';
import { BLOCKCHAIN_URL } from '../config';
import { Logger } from '../common/logger';

const router = express.Router();

// Instantiate new utxo client
const utxoClient = new UtxoClient(BLOCKCHAIN_URL);

const logger = Logger.create('prepare-unspent-outputs-route');

// Define route to prepare unspent outputs
router.get(
  '/api/prepare-unspent-outputs/',
  ErrorHandler.validateRequestParams,
  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { address, amount } = req.query;
    try {
      const result = await utxoClient.prepareUnspentOutputs(
        address as string,
        parseInt(amount as string, 10),
      );
      res.status(200).send(result);
      logger.info(`Route successfully fetched ${result.length} UTXOs for ${address} with the given amount of ${amount} satoshis`);
    } catch (error) {
      logger.error(`Route failed to fetch UTXOs for ${address} with the given amount of ${amount} satoshis`);
      next(error);
    }
  },
);

export { router as prepareUnspentOutputsRouter };
