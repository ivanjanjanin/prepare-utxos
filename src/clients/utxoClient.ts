// Local imports
import UtxoUtil from '../utils/Util';
import HttpClient from '../common/http-client';
import { UnspentOutput, UnspentOutputsResponse } from '../types/utxo';
import { ConflictError, InternalServerError } from '../common/errors';

class UtxoClient {
  private httpClient: HttpClient;

  constructor(baseURL: string) {
    this.httpClient = new HttpClient(baseURL);
  }

  /**
   * This method fetches unspent transaction outputs for the given address.
   * @param {string} address - The blockchain address to get UTXOs for.
   * @returns {Promise<UnspentOutput[]>} A promise that resolves with an array of
   * unspent transaction outputs.
   * @throws {InternalServerError} Will throw an error if the HTTP request fails.
   */
  public async getUnspentOutputs(address: string): Promise<UnspentOutput[]> {
    try {
      const response = await this.httpClient.get<UnspentOutputsResponse>(
        `/unspent?active=${address}`,
      );
      return response.unspent_outputs;
    } catch (error) {
      throw new InternalServerError(
        `An unexpected error happened while fetching UTXOs from external API: ${error}`,
      );
    }
  }

  /**
   * This method prepares the unspent transaction outputs
   * (UTXOs) for a given address and target amount.
   * It applies different strategies sequentially until a suitable
   * set of UTXOs is found or it determines that funds are insufficient.
   * @param {string} address - The address for which UTXOs need to be fetched and prepared.
   * @param {number} amount - The target amount that needs to be matched.
   * @returns {Promise<UnspentOutput[]>} The array of UTXOs that match the target amount.
   * @throws {ConflictError} Will throw an error if there are no UTXOs matching the required amount.
   */
  public async prepareUnspentOutputs(
    address: string,
    amount: number,
  ): Promise<UnspentOutput[]> {
    const unspentOutputs = await this.getUnspentOutputs(address);
    if (!unspentOutputs.length) {
      throw new ConflictError('Not enough funds');
    }

    // Sort the unspentOutputs by value - ascending order
    const sortedOutputs = UtxoUtil.sortUtxosAscending(unspentOutputs);

    // Run zero strategy to try and fetch directly
    const zeroStrategyResult = UtxoUtil.zeroStrategy(sortedOutputs, amount);
    if (zeroStrategyResult.length > 0) {
      return zeroStrategyResult;
    }

    // If there are no exact match in zeroStrategy run firstStrategy
    const firstStrategyResult = UtxoUtil.firstStrategy(sortedOutputs, amount);
    if (firstStrategyResult.length > 0) {
      return firstStrategyResult;
    }

    // If there are no matches in zeroStrategy or firstStrategy run secondStrategy
    const secondStrategyResult = UtxoUtil.secondStrategy(sortedOutputs, amount);
    if (secondStrategyResult.length === 0) {
      throw new ConflictError('Not enough funds');
    }

    return secondStrategyResult;
  }
}

export default UtxoClient;
