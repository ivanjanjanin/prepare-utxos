// External imports
import nock from 'nock';
import request from 'supertest';

// Local imports
import UtxoClient from '../utxoClient';
import { app } from '../../app';
import { BLOCKCHAIN_URL } from '../../config';

const utxoClient = new UtxoClient(BLOCKCHAIN_URL);

// A wallet address used throughout the tests
const address = '31otE5wkfiCSXcQPjfWZ8eLuZRFZfLrmci';

// Mock Blockchain.com API response. A list of outputs with values 3, 8, 6, 9, 1
const unspentOutputs = [
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 0,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 3,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 1,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 8,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 2,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 6,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 3,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 9,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 4,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 1,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
];

beforeEach(() => {
  nock(BLOCKCHAIN_URL)
    .persist()
    .get('/unspent')
    .query({ active: address })
    .reply(200, { unspent_outputs: unspentOutputs });
});

afterEach(() => {
  nock.cleanAll();
});

// Instantiate names for each object for clearer testing
const [unspentOutput0, , unspentOutput2, unspentOutput3, unspentOutput4] = unspentOutputs;

describe('UtxoClient tests', () => {
  it('should fetch UTXOs correctly', async () => {
    const response = await utxoClient.getUnspentOutputs(address);
    expect(response).toEqual(unspentOutputs);
  });

  it('should prepare UTXOs correctly', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address, amount: 4 })
      .send();

    expect(response.status).toBe(200);
    expect(response.body).toEqual([unspentOutput4, unspentOutput0]);
  });

  it('should return the shortest combination for zero strategy', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address, amount: 15 })
      .send();

    expect(response.status).toBe(200);
    expect(response.body).toEqual([unspentOutput2, unspentOutput3]);
  });

  it('should return not enough funds error if the amount is too high', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address, amount: 100 })
      .send();
    expect(response.body).toEqual({ message: 'Not enough funds', status: 409 });
  });

  it('should throw an error for invalid addresses', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address: 'some invalid address', amount: 100 })
      .send();
    expect(response.body).toEqual({ message: 'Invalid bitcoin address', status: 400 });
  });

  it('should throw an error for empty address field', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address: '', amount: 100 })
      .send();
    expect(response.body).toEqual({ message: 'Address is required', status: 400 });
  });

  it('should throw an error for empty amount field', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address, amount: '' })
      .send();
    expect(response.body).toEqual({ message: 'Amount is required', status: 400 });
  });

  it('should throw an error if amount is not a number', async () => {
    const response = await request(app)
      .get('/api/prepare-unspent-outputs')
      .query({ address, amount: 'a' })
      .send();
    expect(response.body).toEqual({ message: 'Amount should be a number', status: 400 });
  });
});
