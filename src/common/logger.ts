/* eslint-disable no-param-reassign */
/* eslint-disable no-useless-constructor */
/* eslint-disable no-unused-vars */
/* eslint-disable no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */

// External imports
import * as winston from 'winston';

// Local imports
import { APP_NAME, LOG_LEVEL, LOG_PATH } from '../config';

export class Logger {
  private static logger: winston.Logger = winston.createLogger({
    level: LOG_LEVEL,
    format: winston.format.combine(
      winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss.SSS',
      }),
      winston.format.splat(),
      winston.format.json(),
    ),
    defaultMeta: { service: APP_NAME, label: 'default' },
    exitOnError: false,
    handleExceptions: true,
    transports: [
      ...(LOG_PATH
        ? [
          new winston.transports.File({
            filename: LOG_PATH ? `${LOG_PATH}/fetcher.log` : undefined,
            silent: LOG_LEVEL === 'silent',
          }),
        ]
        : []),
      new winston.transports.Console({
        format: winston.format.combine(
          winston.format((info) => {
            info.level = info.level.toUpperCase();
            return info;
          })(),

          winston.format.colorize({
            all: true,
          }),
          winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss.SSS',
          }),
          winston.format.printf(
            (info) => `\x1b[36m[${info.service}]\x1b[0m ${info.timestamp}  ${info.level} \x1b[33m[${info.label}]\x1b[0m ${info.message}`,
          ),
        ),
        silent: LOG_LEVEL === 'silent',
      }),
    ],
  });

  constructor(private name: string = 'default') {}

  public static create(name: string): Logger {
    return new Logger(name);
  }

  public static error(message: string, ...meta: any[]): void {
    this.logger.log('error', message, ...meta);
  }

  public static info(message: string, ...meta: any[]): void {
    this.logger.log('info', message, ...meta);
  }

  public error(message: string, ...meta: any[]): void {
    Logger.error(message, ...meta, { label: this.name });
  }

  public info(message: string, ...meta: any[]): void {
    Logger.info(message, ...meta, { label: this.name });
  }
}
