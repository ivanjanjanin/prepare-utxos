// External imports
import axios, { AxiosInstance, AxiosResponse } from 'axios';

class HttpClient {
  private instance: AxiosInstance;

  /**
   * Creates an AxiosInstance with the provided base URL.
   * @param {string} baseURL - The base URL for the HTTP client instance.
   */
  constructor(baseURL: string) {
    this.instance = axios.create({
      baseURL,
    });
  }

  /**
   * Performs a GET request.
   * @param {string} url - The URL for the HTTP GET request.
   * @param {object} params - Optional. The parameters for the HTTP GET request.
   * @returns {Promise<T>} A Promise that resolves with the response data.
   * @template T The type of the response data.
   */
  public async get<T>(url: string, params?: object): Promise<T> {
    const response: AxiosResponse<T> = await this.instance.get(url, { params });
    return response.data;
  }
}

export default HttpClient;
