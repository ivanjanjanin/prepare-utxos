// Local imports
import CustomError from './CustomError';

class InternalServerError extends CustomError {
  constructor(message = 'Internal server error') {
    super(message, 500);
  }
}

export default InternalServerError;
