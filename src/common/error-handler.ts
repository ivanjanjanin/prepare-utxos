// External imports
import WAValidator from 'wallet-address-validator';
import { NextFunction, Request, Response } from 'express';

// Local imports
import { BadRequestError, CustomError } from './errors';

class ErrorHandler {
  /**
   * This method handles custom errors.
   * @param {CustomError} err - The custom error object.
   * @param {Request} req - The Express request object.
   * @param {Response} res - The Express response object.
   */
  static errorHandler(err: Error, req: Request, res: Response) {
    if (err instanceof CustomError) {
      const statusCode = err.status || 500;
      res.status(statusCode).send({ status: statusCode, message: err.message });
    } else {
      res.status(500).send({ status: 500, message: err.message || 'An unexpected error occurred' });
    }
  }

  /**
   * This method validates request parameters for prepare-unspent-outputs route.
   * @param {Request} req - The Express request object.
   * @param {Response} res - The Express response object.
   * @param {NextFunction} next - The next middleware function.
   */
  static validateRequestParams(req: Request, res: Response, next: NextFunction) {
    const { address, amount } = req.query;
    if (!address) {
      next(new BadRequestError('Address is required'));
      return;
    }
    if (!WAValidator.validate(address, 'BTC')) {
      next(new BadRequestError('Invalid bitcoin address'));
      return;
    }
    if (!amount) {
      next(new BadRequestError('Amount is required'));
      return;
    }
    if (Number.isNaN(Number(amount))) {
      next(new BadRequestError('Amount should be a number'));
      return;
    }
    next();
  }
}
export default ErrorHandler;
