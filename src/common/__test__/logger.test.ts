import { Logger } from '../logger';

describe('Logger class', () => {
  it('should create new Logger instance', () => {
    const logger = Logger.create('test-logger');
    expect(logger).toBeInstanceOf(Logger);
  });

  it('should call error method without error', () => {
    const logger = Logger.create('test');
    expect(() => logger.error('error message')).not.toThrow();
  });

  it('should call info method without error', () => {
    const logger = Logger.create('test');
    expect(() => logger.info('info message')).not.toThrow();
  });
});
