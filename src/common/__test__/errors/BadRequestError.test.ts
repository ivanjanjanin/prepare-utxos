// Local imports
import { BadRequestError } from '../../errors';

describe('BadRequestError testing', () => {
  const badRequestError = new BadRequestError();
  it('should have the correct name', () => {
    expect(badRequestError.name).toBe('Error');
  });

  it('should have the correct status code', () => {
    expect(badRequestError.status).toBe(400);
  });

  it('should have a default error message', () => {
    expect(badRequestError.message).toBe('Bad request');
  });

  it('should accept a custom error message', () => {
    const customMessage = 'Custom error message';
    const error = new BadRequestError(customMessage);
    expect(error.message).toBe(customMessage);
  });
});
