// Local imports
import { InternalServerError } from '../../errors';

describe('InternalServerError testing', () => {
  const internalServerError = new InternalServerError();
  it('should have the correct name', () => {
    expect(internalServerError.name).toBe('Error');
  });

  it('should have the correct status code', () => {
    expect(internalServerError.status).toBe(500);
  });

  it('should have a default error message', () => {
    expect(internalServerError.message).toBe('Internal server error');
  });

  it('should accept a custom error message', () => {
    const customMessage = 'Custom error message';
    const error = new InternalServerError(customMessage);
    expect(error.message).toBe(customMessage);
  });
});
