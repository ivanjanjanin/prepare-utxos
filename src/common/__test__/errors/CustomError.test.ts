// Local imports
import { CustomError } from '../../errors';

describe('CustomError testing', () => {
  const customError = new CustomError('Some error message', 999);
  it('should have the correct name', () => {
    expect(customError.name).toBe('Error');
  });

  it('should have the correct status code', () => {
    expect(customError.status).toBe(999);
  });

  it('should have a default error message', () => {
    expect(customError.message).toBe('Some error message');
  });

  it('should accept a custom error message', () => {
    const customMessage = 'Custom error message';
    const customStatus = 5;
    const error = new CustomError(customMessage, customStatus);
    expect(error.message).toBe(customMessage);
    expect(error.status).toBe(customStatus);
  });
});
