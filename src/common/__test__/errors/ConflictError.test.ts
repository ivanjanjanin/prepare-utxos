// Local imports
import { ConflictError } from '../../errors';

describe('ConflictError testing', () => {
  const conflictError = new ConflictError();
  it('should have the correct name', () => {
    expect(conflictError.name).toBe('Error');
  });

  it('should have the correct status code', () => {
    expect(conflictError.status).toBe(409);
  });

  it('should have a default error message', () => {
    expect(conflictError.message).toBe('Conflict');
  });

  it('should accept a custom error message', () => {
    const customMessage = 'Custom error message';
    const error = new ConflictError(customMessage);
    expect(error.message).toBe(customMessage);
  });
});
