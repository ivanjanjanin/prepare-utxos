// External imports
import { Response, Request, NextFunction } from 'express';

// Local imports
import { BadRequestError } from '../errors';
import ErrorHandler from '../error-handler';

describe('errorHandler', () => {
  it('should handle custom errors correctly', () => {
    const err = new BadRequestError('Custom error message');
    const req = {} as Request;
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    } as unknown as Response;

    ErrorHandler.errorHandler(err, req, res);

    expect(res.status).toHaveBeenCalledWith(err.status || 500);
    expect(res.send).toHaveBeenCalledWith({ status: err.status || 500, message: err.message });
  });

  it('should handle non-custom errors correctly', () => {
    const err = new Error('Some error');
    const req = {} as Request;
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    } as unknown as Response;

    ErrorHandler.errorHandler(err, req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.send).toHaveBeenCalledWith({
      status: 500,
      message: err.message || 'An unexpected error occurred',
    });
  });

  describe('validateRequestParams', () => {
    let req: Partial<Request>;
    let res: Partial<Response>;
    let next: jest.Mock;

    beforeEach(() => {
      req = {
        query: {},
      };
      res = {};
      next = jest.fn();
    });

    it('should call the next function if all parameters are present and valid', () => {
      req.query = {
        address: '38XnPvu9PmonFU9WouPXUjYbW91wa5MerL',
        amount: '10',
      };

      ErrorHandler.validateRequestParams(req as Request, res as Response, next as NextFunction);

      expect(next).toHaveBeenCalled();
    });

    it('should call the next function with a BadRequestError if address is missing', () => {
      req.query = {
        amount: '10',
      };

      ErrorHandler.validateRequestParams(req as Request, res as Response, next as NextFunction);

      expect(next).toHaveBeenCalledWith(new BadRequestError('Address is required'));
    });

    it('should call the next function with a BadRequestError if address is invalid', () => {
      req.query = {
        address: 'some-address',
        amount: '10',
      };

      ErrorHandler.validateRequestParams(req as Request, res as Response, next as NextFunction);

      expect(next).toHaveBeenCalledWith(new BadRequestError('Invalid bitcoin address'));
    });

    it('should call the next function with a BadRequestError if amount is missing', () => {
      req.query = {
        address: '38XnPvu9PmonFU9WouPXUjYbW91wa5MerL',
      };

      ErrorHandler.validateRequestParams(req as Request, res as Response, next as NextFunction);

      expect(next).toHaveBeenCalledWith(new BadRequestError('Amount is required'));
    });

    it('should call the next function with a BadRequestError if amount is not a number', () => {
      req.query = {
        address: '38XnPvu9PmonFU9WouPXUjYbW91wa5MerL',
        amount: 'invalid',
      };

      ErrorHandler.validateRequestParams(req as Request, res as Response, next as NextFunction);

      expect(next).toHaveBeenCalledWith(new BadRequestError('Amount should be a number'));
    });
  });
});
