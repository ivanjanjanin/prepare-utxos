// External imports
import nock from 'nock';

// Local imports
import HttpClient from '../http-client';

describe('HttpClient testing', () => {
  const baseURL = 'http://test.com';
  const client = new HttpClient(baseURL);

  afterEach(() => {
    nock.cleanAll();
  });

  it('should perform a GET request and return the expected data', async () => {
    const endpoint = '/test-endpoint';
    const mockData = { key: 'value' };

    nock(baseURL).get(endpoint).reply(200, mockData);

    const data = await client.get(endpoint);

    expect(data).toEqual(mockData);
  });

  it('should pass the correct parameters to the GET request', async () => {
    const endpoint = '/test-endpoint';
    const params = { param: 'test' };
    const mockData = { key: 'value' };

    nock(baseURL).get(endpoint).query(params).reply(200, mockData);

    const data = await client.get(endpoint, params);

    expect(data).toEqual(mockData);
  });
});
