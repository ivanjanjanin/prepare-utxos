/* eslint-disable @typescript-eslint/no-explicit-any */
// External imports
import express from 'express';
import { json } from 'body-parser';

// Local imports
import { prepareUnspentOutputsRouter } from './routes/prepare-unspent-outputs';
import ErrorHandler from './common/error-handler';

const app = express();
app.use(json());
app.use(prepareUnspentOutputsRouter);
// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
  ErrorHandler.errorHandler(err, req, res);
});

export { app };
