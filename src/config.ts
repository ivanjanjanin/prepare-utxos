// External imports
import dotenv from 'dotenv';
import path from 'path';
import fs from 'fs';

const packageInfo = JSON.parse(
  fs.readFileSync(`${path.resolve()}${path.sep}package.json`).toString(),
);

dotenv.config();

export const APP_NAME = packageInfo.name;

export const BLOCKCHAIN_URL = process.env.BLOCKCHAIN_URL ?? '';

export const LOG_LEVEL = process.env.LOG_LEVEL || 'debug';

export const LOG_PATH = process.env.LOG_PATH === 'false' ? undefined : process.env.LOG_PATH || './logs';

export const DOCKER_ENV = process.env.DOCKER_ENV ?? '';
