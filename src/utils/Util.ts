// Local imports
import { BadRequestError } from '../common/errors';
import { UnspentOutput } from '../types/utxo';

class UtxoUtil {
  /**
   * This method sorts an array of UTXOs in ascending order based on their value.
   * @param {UnspentOutput[]} unspentOutputs - The array of UTXOs to sort.
   * @returns {UnspentOutput[]} A new array of UTXOs sorted in ascending order.
   * @throws {BadRequestError} Will throw an error if the unspentOutputs array is empty.
   */
  static sortUtxosAscending(unspentOutputs: UnspentOutput[]): UnspentOutput[] {
    if (!unspentOutputs.length) {
      throw new BadRequestError('Nothing to sort, array should have UTXOs');
    }
    const sorted = unspentOutputs.sort((a, b) => a.value - b.value);
    return sorted;
  }

  /**
   * This method performs a binary search on an array of sorted UnspentOutputs
   * to find an output with a value that exactly matches the provided amount.
   * @param {UnspentOutput[]} unspentOutputs - The array of UnspentOutputs to search through.
   * @param {number} amount - The amount to match against the value of the UnspentOutputs.
   * @returns {object} An object with the found UnspentOutput or null,
   * and the index where the search stopped.
   */
  static findExactMatch(
    unspentOutputs: UnspentOutput[],
    amount: number,
  ): { exactMatch: UnspentOutput | null; start: number } {
    if (!unspentOutputs.length || !amount) {
      throw new BadRequestError('UTXOs and amount should be defined');
    }
    let start = 0;
    let end = unspentOutputs.length - 1;
    let exactMatch = null;

    while (start <= end) {
      const mid = Math.floor((start + end) / 2);

      if (unspentOutputs[mid].value === amount) {
        exactMatch = unspentOutputs[mid];
        break;
      }
      if (unspentOutputs[mid].value < amount) {
        start = mid + 1;
      } else {
        end = mid - 1;
      }
    }

    // If there is no exact match, return null and the final value of start
    return { exactMatch, start };
  }

  /**
   * This method finds a combination of UTXOs that sum to the given amount.
   * It employs a backtracking algorithm if no UTXO/s exactly match the amount.
   * @param {UnspentOutput[]} unspentOutputs - The array of UTXOs to search through.
   * @param {number} amount - The amount to match.
   * @returns {UnspentOutput[][]} An array of UTXOs combinations that sum up to the given amount.
   * @throws {BadRequestError} Will throw an error if the parameters are undefined.
   */
  static findCombination(unspentOutputs: UnspentOutput[], amount: number): UnspentOutput[] {
    if (!unspentOutputs.length || !amount) {
      throw new BadRequestError('UTXOs and amount should be defined');
    }

    // Add check not to look for combinations if the amount is larger than total sum.
    const sum = unspentOutputs.reduce(
      (accumulator, currentOutput) => accumulator + currentOutput.value,
      0,
    );

    if (amount > sum) {
      return [];
    }

    const combinations: UnspentOutput[][] = [];

    const { exactMatch } = this.findExactMatch(unspentOutputs, amount);

    if (exactMatch) {
      return [exactMatch];
    }

    const backtrack = (
      index: number,
      currentCombination: UnspentOutput[],
      currentSum: number,
    ): void => {
      if (currentSum > amount || index >= unspentOutputs.length) {
        return;
      }

      const currentOutput = unspentOutputs[index];

      if (currentSum + currentOutput.value === amount) {
        combinations.push([...currentCombination, currentOutput]);
        return;
      }

      // Include the current output in the combination
      backtrack(
        index + 1,
        [...currentCombination, currentOutput],
        currentSum + currentOutput.value,
      );

      // Exclude the current output from the combination
      backtrack(index + 1, currentCombination, currentSum);
    };

    backtrack(0, [], 0);

    combinations.sort((a, b) => a.length - b.length);

    return combinations.length > 0 ? combinations[0] : [];
  }

  /**
   * This method applies a strategy that finds an exact UTXO match
   * or a sum combination for the amount.
   * @param {UnspentOutput[]} unspentOutputs - The array of UTXOs to apply the strategy on.
   * @param {number} amount - The target amount.
   * @returns {UnspentOutput[]} The UTXO combination matching
   * the amount or an empty array if none found.
   * @throws {BadRequestError} Will throw an error if the parameters are undefined.
   */
  static zeroStrategy(unspentOutputs: UnspentOutput[], amount: number): UnspentOutput[] {
    if (!unspentOutputs.length || !amount) {
      throw new BadRequestError('UTXOs and amount should be defined');
    }

    const combinations = this.findCombination(unspentOutputs, amount);

    return combinations.length > 0 ? combinations : [];
  }

  /**
   * This method selects UTXOs using the first strategy: it selects UTXOs from
   * sorted array in order while the accumulated value is less than
   * the amount needed, and each UTXO value is also less than the amount needed.
   * @param {UnspentOutput[]} unspentOutputs - The array of UTXOs to select from.
   * @param {number} amount - The amount needed.
   * @returns {UnspentOutput[]} An array of selected UTXOs.
   * @throws {BadRequestError} Will throw an error if the parameters are undefined.
   */
  static firstStrategy(unspentOutputs: UnspentOutput[], amount: number): UnspentOutput[] {
    if (!unspentOutputs.length || !amount) {
      throw new BadRequestError('UTXOs and amount should be defined');
    }

    let currentAmount = 0;
    const selectedOutputs: UnspentOutput[] = [];

    // Iterate through the sorted unspentOutputs from smallest to largest value
    for (const output of unspentOutputs) {
      if (output.value < amount) {
        selectedOutputs.push(output);
        currentAmount += output.value;
      }

      if (currentAmount >= amount) {
        break;
      }
    }

    return currentAmount < amount ? [] : selectedOutputs;
  }

  /**
   * This method selects UTXOs using the second strategy: it returns the first UTXO
   * in a sorted array whose value is equal to or greater than the amount needed.
   * @param {UnspentOutput[]} utxos - The array of UTXOs to select from.
   * @param {number} amount - The amount needed.
   * @returns {UnspentOutput[]} An array with a single selected UTXO,
   * or an empty array if no such UTXO exists.
   * @throws {BadRequestError} Will throw an error if the parameters are undefined.
   */
  static secondStrategy(unspentOutputs: UnspentOutput[], amount: number): UnspentOutput[] {
    if (!unspentOutputs.length || !amount) {
      throw new BadRequestError('UTXOs and amount should be defined');
    }

    const { exactMatch, start } = this.findExactMatch(unspentOutputs, amount);

    // If there is no exact match return first that is above the requested amount
    if (!exactMatch && start < unspentOutputs.length && unspentOutputs[start].value > amount) {
      return [unspentOutputs[start]];
    }

    return exactMatch ? [exactMatch] : [];
  }
}
export default UtxoUtil;
