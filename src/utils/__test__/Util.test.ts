// Local imports
import { BadRequestError } from '../../common/errors';
import UtxoUtil from '../Util';

// Mock Blockchain.com API response. A list of outputs with values 3, 8, 6, 9, 1
const unspentOutputs = [
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 0,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 3,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 1,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 8,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 2,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 6,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 3,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 9,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
  {
    tx_hash_big_endian: 'f35b0fd3954cfdc817cf1a2747236b3a105886a986fbbfbb222b0af1584333fa',
    tx_hash: 'fa334358f10a2b22bbbffb86a98658103a6b2347271acf17c8fd4c95d30f5bf3',
    tx_output_n: 4,
    script: 'a914014c7c73f43e33a099f915761cd0ff5d1145183287',
    value: 1,
    value_hex: '040356',
    confirmations: 1095,
    tx_index: 8803138565710149,
  },
];

// Instantiate names for each object for clearer testing
const [
  unspentOutput0,
  unspentOutput1,
  unspentOutput2,
  unspentOutput3,
  unspentOutput4] = unspentOutputs;

// Sort UTXOs in ascending order to give to each function
const sortedOutputs = UtxoUtil.sortUtxosAscending(unspentOutputs);

describe('UtxoUtil tests', () => {
  it('should sort UTXOs in ascending order correctly', () => {
    const sortedAscending = UtxoUtil.sortUtxosAscending(unspentOutputs);
    expect(sortedAscending).toEqual([
      unspentOutput4,
      unspentOutput0,
      unspentOutput2,
      unspentOutput1,
      unspentOutput3,
    ]);

    expect(() => {
      UtxoUtil.sortUtxosAscending([]);
    }).toThrowError(new BadRequestError('Nothing to sort, array should have UTXOs'));
  });

  it('should find the exact match if one exists', () => {
    let result = UtxoUtil.findExactMatch(unspentOutputs, 9);
    expect(result.exactMatch).toEqual(unspentOutput3);

    result = UtxoUtil.findExactMatch(unspentOutputs, 7);
    expect(result.exactMatch).toEqual(null);
    expect(result.start).toEqual(3);

    expect(() => {
      UtxoUtil.findExactMatch(sortedOutputs, 0);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));

    expect(() => {
      UtxoUtil.findExactMatch([], 9);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));
  });

  it('should find a correct combination of UTXOs for a given amount', () => {
    let expectedUTXOs = UtxoUtil.findCombination(sortedOutputs, 7);
    expect(expectedUTXOs).toEqual([unspentOutput4, unspentOutput2]);

    expectedUTXOs = UtxoUtil.findCombination(sortedOutputs, 8);
    expect(expectedUTXOs).toEqual([unspentOutput1]);

    expect(() => {
      UtxoUtil.findCombination(sortedOutputs, 0);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));

    expect(() => {
      UtxoUtil.findCombination([], 5);
    }).toThrow(new BadRequestError('UTXOs and amount should be defined'));
  });

  it('should select UTXOs with zero strategy correctly', () => {
    let expectedUTXOs = UtxoUtil.zeroStrategy(sortedOutputs, 7);
    expect(expectedUTXOs).toEqual([unspentOutput4, unspentOutput2]);

    expectedUTXOs = UtxoUtil.zeroStrategy(sortedOutputs, 8);
    expect(expectedUTXOs).toEqual([unspentOutput1]);

    expect(() => {
      UtxoUtil.zeroStrategy(sortedOutputs, 0);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));

    expect(() => {
      UtxoUtil.zeroStrategy([], 5);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));
  });

  it('should select UTXOs with first strategy correctly', () => {
    const expectedUTXOs = UtxoUtil.firstStrategy(sortedOutputs, 4);
    expect(expectedUTXOs).toEqual([unspentOutput4, unspentOutput0]);

    expect(() => {
      UtxoUtil.firstStrategy(sortedOutputs, 0);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));

    expect(() => {
      UtxoUtil.firstStrategy([], 5);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));
  });

  it('should select UTXOs with second strategy correctly', () => {
    const expectedUTXOs = UtxoUtil.secondStrategy(sortedOutputs, 5);
    expect(expectedUTXOs).toEqual([unspentOutput2]);

    expect(() => {
      UtxoUtil.secondStrategy(sortedOutputs, 0);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));

    expect(() => {
      UtxoUtil.secondStrategy([], 5);
    }).toThrowError(new BadRequestError('UTXOs and amount should be defined'));
  });
});
