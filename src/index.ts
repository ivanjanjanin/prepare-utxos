// Local imports
import { Logger } from './common/logger';
import { app } from './app';
import { DOCKER_ENV } from './config';

const logger = Logger.create('Zumo-backend-app');

app.listen(DOCKER_ENV ? 4000 : 3000, () => {
  if (DOCKER_ENV) {
    logger.info('Running in Docker. Listening on port 4000!');
  } else {
    logger.info('Running locally. Listening on port 3000!');
  }
});
