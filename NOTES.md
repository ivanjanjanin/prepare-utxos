# Application Startup Guide

This application is designed to prepare unspent transaction outputs (UTXOs) for a given address and target amount on the blockchain.
This document describes two ways to start the application: `Local Installation` and `Docker Installation`.

This application can be started in two ways:

## Method 1: Local Installation

1. Open the root directory of this project in your terminal.

2. Install the necessary dependencies by running the following command:

```bash
npm install
```

3. Rename `.env.example` file to `.env`.

4. Start the application by running the following command:

```bash
npm start
```

- This command will start the application on `port 3000` via the `index.js` file.

5. Once the application is running, you can send a request to the application.

- Here is an example:

```bash
curl "http://localhost:3000/api/prepare-unspent-outputs/?address=YOUR_ADDRESS&amount=AMOUNT"
```

- Replace `YOUR_ADDRESS` and `AMOUNT` with the address and the amount you want to fetch the unspent transaction outputs for.

## Method 2: Docker Installation

1. Open the root directory of this project in your terminal.

2. Rename `docker.env.example` file to `docker.env`.

3. Start the Docker image by running the following command:

```bash
docker-compose up
```

- This command will start the application inside the Docker container on `port 4000`, and map it to `port 4000` on your host machine.

4. Once the Docker container is up and running, you can send a request to the application.

- Here is an example:

```bash
curl "http://localhost:4000/api/prepare-unspent-outputs/?address=YOUR_ADDRESS&amount=AMOUNT"
```

- Replace `YOUR_ADDRESS` and `AMOUNT` with the address and the amount you want to fetch the unspent transaction outputs for.

---

Remember to have both  Docker and Docker compose installed on your machine to use the second method. For the first method, you need Node.js and npm installed on your machine.
