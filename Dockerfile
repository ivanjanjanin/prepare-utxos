# Use the official Node.js 16 image
FROM node:16

# Set the working directory in the container to /usr/src/app
WORKDIR /usr/src/app

# Copy both package.json and package-lock.json (if available) to the working directory
COPY package*.json ./

# Install the application dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Build the application
RUN npm run build

# Inform Docker that the container listens on port 4000 at runtime
EXPOSE 4000

# The command that starts the application
CMD [ "node", ".build/index.js" ] 
